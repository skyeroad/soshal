//
//  MLViewController.m
//  Soshal
//
//  Created by Matt Long on 3/4/14.
//  Copyright (c) 2014 Matt Long. All rights reserved.
//

#import "MLViewController.h"

#define kServerURL @"https://healthdare.herokuapp.com/"
//#define kServerURL @"http://0.0.0.0:3030/"

@interface MLViewController ()
@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *fbAccount;
@property (nonatomic, copy) NSString *xcsrfToken;
@end

@implementation MLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.accountStore = [[ACAccountStore alloc] init];

}

- (IBAction)doIt:(id)sender
{
    // Get the Facebook account type for the access request
    ACAccountType *fbAccountType = [self.accountStore
                                    accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *fbInfo = @{
//                             ACFacebookAppIdKey: @"593231207428474", // local
                             ACFacebookAppIdKey: @"464090973719090",  // production
                              ACFacebookPermissionsKey: @[@"basic_info"],
                              ACFacebookAudienceKey: ACFacebookAudienceFriends
                              };
    
    __weak MLViewController *weakSelf = self;
    // Request access to the Facebook account with the access info
    [self.accountStore requestAccessToAccountsWithType:fbAccountType
                                               options:fbInfo
                                            completion:^(BOOL granted, NSError *error) {
                                                if (granted) {
                                                    // If access granted, then get the Facebook account info
                                                    NSArray *accounts = [self.accountStore
                                                                         accountsWithAccountType:fbAccountType];
                                                    self.fbAccount = [accounts lastObject];
                                                    
                                                    // Get the access token, could be used in other scenarios
                                                    ACAccountCredential *fbCredential = [self.fbAccount credential];            
                                                    NSString *accessToken = [fbCredential oauthToken];
                                                    NSLog(@"Facebook Access Token: %@", accessToken);
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [weakSelf loginNowThatYouHaveThisToken:accessToken];
                                                    });
                                                    
                                                    // Add code here to make an API request using the SLRequest class
                                                    
                                                } else {
                                                    NSLog(@"Access not granted");
                                                }
                                            }];
    
}

- (void)loginNowThatYouHaveThisToken:(NSString*)authToken
{
    NSString *postUrlString = [NSString stringWithFormat:@"%@auth/facebook_access_token/callback", kServerURL];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:postUrlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0f];
    [request setHTTPMethod:@"POST"];
    NSString *params = [NSString stringWithFormat:@"access_token=%@", authToken];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        self.xcsrfToken = httpResp.allHeaderFields[@"X-CSRF-Token"];
        NSLog(@"Headers: %@", httpResp.allHeaderFields);
        NSLog(@"%@", json);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self fetchMyDares];
            [self postJournalEntries];
        });
    }];
    
    [dataTask resume];
}

- (void)fetchMyDares
{
    NSString *urlString = [NSString stringWithFormat:@"%@dares.json", kServerURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0f];
    [request setHTTPShouldHandleCookies:YES];
    
//    [request setValue:self.xcsrfToken forHTTPHeaderField:@"X-CSRF-Token"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", json);
    }];
    
    [dataTask resume];
    
}

- (void)postJournalEntries
{
//    NSString *postUrlString = [NSString stringWithFormat:@"%@dares/6/teams/6/journal_entries.json", kServerURL];
    NSString *postUrlString = [NSString stringWithFormat:@"%@dares/4/teams/5/journal_entries.json", kServerURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:postUrlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0f];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];

    NSDictionary *entries = @{@"journal_entries" : @[
                                      @{
                                          @"journal_entry_type_name": @"Water",
                                          @"shared" : @(1),
                                          @"text_description" : @"I had a little water"
                                        },
                                      @{
                                          @"journal_entry_type_name": @"Meal",
                                          @"shared" : @(0),
                                          @"text_description" : @"I ate a tastey taco at the Taco Bell!"
                                          }
                                      ]};
    
    NSData *body = [NSJSONSerialization dataWithJSONObject:entries options:0 error:nil];
    [request setHTTPBody:body];
    
    // We need to pass this header for POSTS
    [request setValue:self.xcsrfToken forHTTPHeaderField:@"X-CSRF-Token"];

    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }];
    
    [dataTask resume];
    
}

@end
