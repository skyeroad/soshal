//
//  MLAppDelegate.h
//  Soshal
//
//  Created by Matt Long on 3/4/14.
//  Copyright (c) 2014 Matt Long. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
